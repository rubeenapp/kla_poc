package com.kla.api.part2bulletin;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.kla.api.model.AuditModel;

@Entity
@Table(name = "Part2Bulletin")
public class Part2Bulletin extends AuditModel {

	@Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id")
    private UUID id;

	@Column
	private Long bulletinNumber;
	
	@OneToMany(mappedBy = "part2Bulletin")
    private List<BulletinLines> bills = new ArrayList<BulletinLines>();

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Long getBulletinNumber() {
		return bulletinNumber;
	}

	public void setBulletinNumber(Long bulletinNumber) {
		this.bulletinNumber = bulletinNumber;
	}

	public List<BulletinLines> getBills() {
		return bills;
	}

	public void setBills(List<BulletinLines> bills) {
		this.bills = bills;
	}	
}
