package com.kla.api.amendment;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.kla.api.exception.ResourceNotFoundException;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/kla/api")
public class AmendmentController {

	@Autowired
	private AmendmentRepository amendmentRepository;

	@PostMapping("/amendment")
	public ResponseEntity<String> createamendment(@RequestBody Amendment amendment) {
		amendmentRepository.save(amendment);
		return ResponseEntity.ok().body("Amendment created successfully!");
	}

	@GetMapping("/amendments") 
	public List<Amendment> getamendments() { 
		return amendmentRepository.findAll(); 
	}

	@PutMapping("/amendment/{amendmentId}") 
	public ResponseEntity<String> updateamendment(@Valid @RequestParam Amendment amendment) {
		amendmentRepository.save(amendment);
		return ResponseEntity.ok().body("Amendment updated successfully!");
	}	

	@GetMapping("/amendment/{amendmentId}")
	public ResponseEntity<?> getamendment(@PathVariable Long amendmentId) {
		return amendmentRepository.findById(amendmentId)
				.map(amendment -> {
					return ResponseEntity.ok(amendment);
				}).orElseThrow(() -> new ResourceNotFoundException("amendment not found with id " + amendmentId));
	}	

	@DeleteMapping("/amendment/{amendmentId}")
	public ResponseEntity<?> deleteamendment(@PathVariable Long amendmentId) {
		return amendmentRepository.findById(amendmentId)
				.map(amendment -> {
					amendmentRepository.delete(amendment);
					return ResponseEntity.ok().body("Amendment deleted successfully!");
				}).orElseThrow(() -> new ResourceNotFoundException("amendment not found with id " + amendmentId));
	}	
}
