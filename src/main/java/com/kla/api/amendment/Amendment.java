package com.kla.api.amendment;

import java.util.UUID;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.kla.api.bill.Bill;
import com.kla.api.model.AuditModel;
import com.kla.api.user.User;

@Entity
@Table(name = "BillAmendment")
public class Amendment extends AuditModel {

	@Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id")
    private UUID id;

	@ManyToOne
	private Bill bill;

	@Column(columnDefinition = "TEXT") 
	private String content;

	@Column
	private String status;

	@OneToOne
	private User user;	

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
