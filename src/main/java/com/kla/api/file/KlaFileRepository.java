package com.kla.api.file;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KlaFileRepository extends JpaRepository<KlaFile, Long> {	
}