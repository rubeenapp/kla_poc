package com.kla.api.bill;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.kla.api.amendment.Amendment;
import com.kla.api.file.KlaFile;
import com.kla.api.model.AuditModel;
import com.kla.api.part2bulletin.BulletinLines;
import com.kla.api.user.User;


@Entity
@Table(name = "klaBill")
public class Bill extends AuditModel {

	@Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id")
    private UUID id;
	
	@Column(unique=true)
	private long billNumber;
	
	@Column(columnDefinition = "TEXT") 
	private String content;
	
	@Column
	private String billPriority;
	
	@Column
	private String vettingStatus;
	
	@Column
	private String bacStatus;
	
	@Column 
	private String scStatus;
	
	@Column
	private String ldNumber;
	
	@ManyToOne
	private User user2;
	
	@OneToMany(mappedBy = "bill")
    private List<Amendment> amendment = new ArrayList<Amendment>();
	
	@ManyToOne
	private KlaFile kFile;
	
	@OneToOne(mappedBy = "blbill")
	private BulletinLines bulletinLines;
	
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public long getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(long billNumber) {
		this.billNumber = billNumber;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getBillPriority() {
		return billPriority;
	}

	public void setBillPriority(String billPriority) {
		this.billPriority = billPriority;
	}

	public String getVettingStatus() {
		return vettingStatus;
	}

	public void setVettingStatus(String vettingStatus) {
		this.vettingStatus = vettingStatus;
	}

	public String getBacStatus() {
		return bacStatus;
	}

	public void setBacStatus(String bacStatus) {
		this.bacStatus = bacStatus;
	}

	public String getScStatus() {
		return scStatus;
	}

	public void setScStatus(String scStatus) {
		this.scStatus = scStatus;
	}

	public String getLdNumber() {
		return ldNumber;
	}

	public void setLdNumber(String ldNumber) {
		this.ldNumber = ldNumber;
	}
	
	public BulletinLines getBulletinLines() {
		return bulletinLines;
	}

	public void setBulletinLines(BulletinLines bulletinLines) {
		this.bulletinLines = bulletinLines;
	}

	public KlaFile getkFile() {
		return kFile;
	}

	public void setkFile(KlaFile kFile) {
		this.kFile = kFile;
	}

	public User getUser2() {
		return user2;
	}

	public void setUser2(User user2) {
		this.user2 = user2;
	}

	public List<Amendment> getAmendment() {
		return amendment;
	}

	public void setAmendment(List<Amendment> amendment) {
		this.amendment = amendment;
	}
	
}
